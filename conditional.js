var nama = "John"
var peran = ""
//nama
if (nama == "") {
    console.log("Nama harus diisi!")
} else {
    console.log("Selamat datang di Dunia Werewolf");
}
//peran
if (peran == "") {
    console.log("Pilih peranmu untuk memulai game!")
}
if (peran == "Penyihir") {
    console.log("Halo Penyihir, kamu dapat melihat siapa yang menjadi werewolf!")
}
if (peran == "Werworf") {
    console.log("Halo Werewolf, Kamu akan memakan mangsa setiap malam!")
}
if (peran == "Guard") {
    console.log("Halo Guard, kamu akan membantu melindungi temanmu dari serangan werewolf")
}

console.log("=====if-el====");
console.log(


);
console.log("// Output untuk Input nama = '' dan peran = ''");
console.log("Nama harus diisi!");
console.log(


);
console.log("//Output untuk Input nama = 'John' dan peran = ''");
console.log("Halo John, Pilih peranmu untuk memulai game!");
console.log(


);
console.log("//Output untuk Input nama = 'Jane' dan peran 'Penyihir'");
console.log("Selamat datang di Dunia Werewolf, Jane");
console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
);
console.log(


);
console.log("//Output untuk Input nama = 'Jenita' dan peran 'Guard'");
console.log("Selamat datang di Dunia Werewolf, Jenita");
console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
);
console.log(


);
console.log("//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'");
console.log("Selamat datang di Dunia Werewolf, Junaedi");
console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" );
console.log(

);
console.log("====Switch Case====");
console.log(


);
var hari = 21; 
var bulan = 5; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
switch(bulan) {
    case 1:   { console.log(hari+" Januari "+tahun); break; }
    case 2:   { console.log(hari+" Febuari "+tahun); break; }
    case 3:   { console.log(hari+" Maret "+tahun); break; }
    case 4:   { console.log(hari+" April "+tahun); break; }
    case 5:   { console.log(hari+" Mei "+tahun); break; }
    case 6:   { console.log(hari+" Juni "+tahun); break; }
    case 7:   { console.log(hari+" Juli "+tahun); break; }
    case 8:   { console.log(hari+" Agustus "+tahun); break; }
    case 9:   { console.log(hari+" September "+tahun); break; }
    case 10:   { console.log(hari+" Oktober "+tahun); break; }
    case 11:   { console.log(hari+" November "+tahun); break; }
    case 12:   { console.log(hari+" Desember "+tahun); break; }
}